/*
 * main.js -- CTM Player JavaScript file.
 *
 * @license Copyright 2013 - 2021 Felipe Peñailillo <fcpc.1984@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Placeholder for template generator
var tmpl;

window.debugCTMPlayer = function(msg) {
  if (Object.prototype.hasOwnProperty.call(window.CTMPlayer, "debug") && window.CTMPlayer.debug) {
    if (document.querySelector("#ctm-player-console")) {
      var consoleElement = document.getElementById("ctm-player-console"),
        elementToAppend = document.createElement("div"),
        now = new Date(Date.now());
      elementToAppend.append(now.getDate().lead0(2) + "/" + (now.getMonth() + 1).lead0(2) + "/" + now.getFullYear() + " " + now.getHours().lead0(2) + ":" + now.getMinutes().lead0(2) + ":" + now.getSeconds().lead0(2) + "." + now.getMilliseconds().lead0(3) + ": " + msg);
      if (consoleElement.childNodes.length >= 1000) {
        consoleElement.removeChild(consoleElement.childNodes[0]);
      }
      consoleElement.appendChild(elementToAppend);
      consoleElement.scroll(0, consoleElement.scrollHeight);
    } else {
      console.debug(msg);
    }
  }
};

Number.prototype.lead0 = function(n) {
  var nz = "" + this;
  while (nz.length < n) {
    nz = "0" + nz;
  }
  return nz;
};

function ready(fn) {
  if (document.readyState != "loading") {
    fn();
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

window.CTMPlayer = function(options) {
  this.options = options;
  this.bootstrapPlayer();
};

window.CTMPlayer.version = "0.4.0";

window.CTMPlayer.prototype = {
  bootstrapPlayer: function() {
    if (this.options.selector === undefined) {
      console.error("You must indicate a selector!", this.audio);
      return false;
    } else {
      this.audio = document.getElementById(this.options.selector);
      if (this.audio.currentSrc === "" && this.audio.src === undefined && !this.options.src) {
        console.error("You must indicate at least an URL.", this.audio);
        return false;
      }
      if (!this.audio.hasAttribute("preload") || (this.audio.hasAttribute("preload") && (this.audio.preload === "auto" || this.audio.preload === ""))) {
        console.warn("The player could be loading data at this time. If it is an audio stream, it is very likely that what is being played is not the most recent.", this.audio);
      }
      if (this.audio.preload === "metadata") {
        console.warn("Playback may not behave as expected in older browsers.", this.audio);
      }
      if (this.audio.hasAttribute("autoplay")) {
        console.warn("The autoplay attribute will be ignored if there is no previous interaction by the user.", this.audio);
      }
      if (this.audio.controls) {
        console.info("The native player will appear above the Player.", this.audio);
      }
    }
    if (Object.prototype.hasOwnProperty.call(this.options, "events") && !Object.prototype.toString.call(this.options.events) === "[object Object]") {
      console.error("Events argument object MUST be \"plain\"", this.audio);
      return false;
    }
    if (Object.prototype.hasOwnProperty.call(this.options, "template")) {
      console.info("A custom template will be loaded.", this.audio);
    }
    if (Object.prototype.hasOwnProperty.call(this.options, "mode")) {
      this.audio.mode = this.options.mode;
    } else {
      this.detectMediaMode();
    }
    if (this.audio.mode === "stream") {
      let url = new URL(this.audio.src),
        urlParams = new URLSearchParams(url.search);
      urlParams.set("cb", new Date().getTime());
      url.search = urlParams.toString();
      this.source = this.audio.src = url.toString();
    }
    this.container = document.createElement("div");
    this.container.id = "ctm-player-" + this.options.selector;
    this.container.className = (this.options.className !== undefined ? this.options.className : "ctm-player-default-skin") + " ctm-player-container";
    if (this.audio.nextSibling) {
      this.audio.parentNode.insertBefore(this.container, this.audio.nextSibling);
    } else {
      this.audio.parentNode.appendChild(this.container);
    }
    // Storing the <audio> element before building the final HTML
    this.audio = this.audio.parentNode.removeChild(this.audio);
    document.getElementById("ctm-player-" + this.options.selector).innerHTML = this.createHTML(this.options.template);
    // Assigning the <audio> element which is now visible.
    this.audio = document.getElementById(this.options.selector);
    this.initPlayer();
    this.prepareUnloadPlayer();
  },
  createHTML: function(template) {
    if (template === undefined && document.getElementById("ctm-player-template") === null) {
      var tpl = document.createElement("script");
      tpl.type = "text/x-tmpl";
      tpl.id = "ctm-player-template";
      tpl.innerHTML = "<div class=\"ctm-player-html5-player\">" +
        "{%#o.audio%}" +
        "</div>" +
        "<div class=\"ctm-player-main\">" +
        "<div class=\"ctm-player-left-buttons\">" +
        "<button class=\"ctm-player-play ctm-player-button\">" +
        "<i class=\"icon-play\"></i>" +
        "</button>" +
        "<button class=\"ctm-player-pause ctm-player-button\" hidden>" +
        "<i class=\"icon-pause\"></i>" +
        "</button>" +
        "<button class=\"ctm-player-stop ctm-player-button\" hidden>" +
        "<i class=\"icon-stop\"></i>" +
        "</button>" +
        "</div>" +
        "<div class=\"ctm-player-center-status\">" +
        "<div class=\"ctm-player-progress ctm-player-progress-streaming\">" +
        "<div class=\"ctm-player-progress-bar\"></div>" +
        "<div class=\"ctm-player-progress-download\"></div>" +
        "</div>" +
        "<div class=\"ctm-player-time\">" +
        "<span class=\"ctm-player-days\"></span> " +
        "<span class=\"ctm-player-hours\"></span>" +
        "<span class=\"ctm-player-minutes\">00:</span>" +
        "<span class=\"ctm-player-seconds\">00</span>" +
        "</div>" +
        "</div>" +
        "<div class=\"ctm-player-right-controls" +
        "{% if (o.sources.length > 0) { %}" +
        " ctm-player-dropdown\"" +
        "{% } else { %}" +
        "\" style=\"overflow:hidden;" +
        "{% } %}" +
        "\">" +
        "<button class=\"ctm-player-volume-button ctm-player-button\">" +
        "<i class=\"icon-volume-up\"></i>" +
        "</button>" +
        "<input class=\"ctm-player-volume-slider\" type=\"range\" max=\"1\" step=\"0.01\" value=\"1\" oninput=\"{%=o.selector%}.volume=this.value;\">" +
        "{% if (o.sources.length > 0) { %}" +
        "<button class=\"ctm-player-config ctm-player-button\">" +
        "<i class=\"icon-cog\"></i>" +
        "</button>" +
        "<ul class=\"ctm-player-source-select\" hidden>" +
        "<li class=\"ctm-player-source-select-header\">" +
        "<i class=\"icon-music\"></i>{%=o.source_message%}" +
        "</li>" +
        "{% for (var i=0; i<o.sources.length; i++) { %}" +
        "<li class=\"ctm-player-source-select-item\"><a " +
        "href=\"{%=o.sources[i].url%}\" data-mode=\"{%=o.sources[i].mode%}\"> " +
        "{% if (o.sources[i].icon !== undefined) { %}" + "<i class=\"{%=o.sources[i].icon%}\"></i> " + "{% } %}" +
        "<span>{%=o.sources[i].name%}</span>" +
        "<i class=\"icon-ok\" {% if (i != 0) { %}" + "hidden" + "{% } %}></i>" +
        "</a></li>" +
        "{% } %}" +
        "</ul>" +
        "{% } %}" +
        "</div>" +
        "</div>";
      document.body.appendChild(tpl);
    }
    return tmpl(template !== undefined ? template : "ctm-player-template", {
      selector: this.options.selector,
      audio: this.audio.outerHTML,
      sources: this.options.sources ? this.options.sources : [],
      source_message: this.options.source_message ? this.options.source_message : "Select a source"
    });
  },
  initPlayer: function() {
    if (document.createElement("audio").canPlayType) {
      var audio = document.createElement("audio");
      var self = this;
      if (audio.canPlayType && audio.canPlayType("audio/ogg; codecs=\"vorbis\"").replace(/no/, "")) {
        if (this.container.querySelector(".ctm-player-minutes")) {
          this.container.querySelector(".ctm-player-minutes").textContent = "00:";
        }
        if (this.container.querySelector(".ctm-player-seconds")) {
          this.container.querySelector(".ctm-player-seconds").textContent = "00";
        }
        this.container.querySelector(".ctm-player-play").addEventListener("click", function() {
          if (self.audio.paused) {
            self.audio.play();
            window.debugCTMPlayer("Starting playback...");
          } else {
            self.resetPlayer();
          }
          self.container.querySelector(".ctm-player-play").setAttribute("hidden", "");
          if (self.audio.mode === undefined) {
            self.detectMediaMode();
          }
          if (self.audio.mode === "static") {
            self.container.querySelector(".ctm-player-pause").removeAttribute("hidden");
            self.container.querySelector(".ctm-player-stop").removeAttribute("disabled");
          } else {
            self.container.querySelector(".ctm-player-stop").removeAttribute("hidden");
          }
        });
        if (this.container.querySelector(".ctm-player-pause")) {
          this.container.querySelector(".ctm-player-pause").addEventListener("click", function() {
            // This is fail-safe logic, player MUST have audio.pause value as false at this point
            if (!self.audio.paused) {
              self.audio.pause();
              self.container.querySelector(".ctm-player-pause").setAttribute("hidden", "");
              self.container.querySelector(".ctm-player-play").removeAttribute("hidden");
            }
          });
        }
        this.container.querySelector(".ctm-player-stop").addEventListener("click", function() {
          if (!this.hasAttribute("disabled")) {
            // NOTE: If detectMediaMode() is unable to detect audio mode, it will
            // try to detect based on the <audio> element declaration
            if (self.audio.mode === "stream" && !self.audio.paused || (self.audio.hasAttribute("preload") && self.audio.preload === "none")) {
              self.container.querySelector(".ctm-player-stop").setAttribute("hidden", "");
              self.audio.wasPlaying = false;
              self.resetPlayer();
            } else if (self.audio.mode === "static" || (self.audio.hasAttribute("preload") && self.audio.preload === "metadata")) {
              self.audio.pause();
              self.audio.currentTime = 0;
              self.container.querySelector(".ctm-player-pause").setAttribute("hidden", "");
              self.container.querySelector(".ctm-player-stop").removeAttribute("hidden");
              self.container.querySelector(".ctm-player-stop").setAttribute("disabled", "");
            }
            self.container.querySelector(".ctm-player-play").removeAttribute("hidden");
            self.audio.wasPlaying = false;
          }
        });
        this.audio.wasPlaying = false;
        this.addEvents();
        if (this.container.querySelector(".ctm-player-center-status .ctm-player-progress")) {
          this.container.querySelector(".ctm-player-center-status .ctm-player-progress").addEventListener("click", function(event) {
            if (self.audio.mode === "static") {
              let rect = this.getBoundingClientRect();
              var position = ((event.pageX - rect.left + document.body.scrollLeft) / parseFloat(getComputedStyle(this, null).width.replace("px", ""))) * 100;
              if (position < 0) {
                position = 0;
              }
              if (position > 100) {
                position = 100;
              }
              self.audio.currentTime = self.audio.duration * (position / 100);
            }
          });
        }
        if (this.container.querySelector(".ctm-player-volume-button")) {
          this.container.querySelector(".ctm-player-volume-button").addEventListener("click", function() {
            this.querySelector("i").classList.toggle("icon-volume-up");
            this.querySelector("i").classList.toggle("icon-volume-off");
            self.audio.muted = !self.audio.muted;
            window.debugCTMPlayer("audio.muted is '" + self.audio.muted + "'.");
          });
        }
        this.container.addEventListener("selectstart", function(e) {
          e.preventDefault();
          return false;
        });
        if (this.container.querySelector(".ctm-player-config")) {
          this.container.querySelector(".ctm-player-config").addEventListener("click", function() {
            self.container.querySelector(".ctm-player-source-select").toggleAttribute("hidden");
          });
        }
        this.container.querySelectorAll(".ctm-player-source-select a").forEach(function(element) {
          element.addEventListener("click", function(e) {
            e.preventDefault();
            if (element.querySelector("i.icon-ok").hasAttribute("hidden")) {
              self.container.querySelectorAll(".ctm-player-source-select a i.icon-ok").forEach(function(el) {
                el.setAttribute("hidden", "");
              });
              element.querySelector("i.icon-ok").removeAttribute("hidden");
              self.source = element.href;
              self.audio.mode = element.dataset.mode;
              // Cleaning progress
              let progress = self.container.querySelector(".ctm-player-progress-download");
              while (progress.firstChild)
                progress.removeChild(progress.firstChild);
              self.resetPlayer();
            }
          });
        });
        window.debugCTMPlayer("Initialization complete.");
      } else {
        this.container.classList.add("ctm-player-cant-ogg");
        this.container.innerHTML = "This browser does not support the free Ogg codec.";
        window.debugCTMPlayer("This browser does not support the free Ogg codec.");
      }
    } else {
      this.container.classList.add("ctm-player-cant-html5");
      this.container.innerHTML = "This browser does not support the HTML5 <audio> tag or has it disabled.";
      window.debugCTMPlayer("This browser does not support the HTML5 <audio> tag or has it disabled.");
    }
  },
  _eventHandlers: Object(),
  addEvent: function(node, event, handler, capture) {
    if (!(node in this._eventHandlers)) {
      // _eventHandlers stores references to nodes
      this._eventHandlers[node] = {};
    }
    if (!(event in this._eventHandlers[node])) {
      // each entry contains another entry for each event type
      this._eventHandlers[node][event] = [];
    }
    // OPTIONAL: capture reference
    this._eventHandlers[node][event].push([handler, capture]);
    node.addEventListener(event, handler, capture);
  },
  removeAllEvents: function(node) {
    if (node in this._eventHandlers) {
      var handlers = this._eventHandlers[node];
      for (let evnt in handlers) {
        var eventHandlers = handlers[evnt];
        for (var i = eventHandlers.length; i--;) {
          var handler = eventHandlers[i];
          node.removeEventListener(evnt, handler[0], handler[1]);
        }
      }
    }
  },
  addEvents: function() {
    let self = this;
    // Assigning custom events first
    if (this.options.events !== undefined) {
      for (const [key, func] of Object.entries(this.options.events)) {
        this.addEvent(this.audio, key, func, false);
      }
    }
    this.addEvent(this.audio, "abort", function() {
      window.debugCTMPlayer("'abort' event triggered.");
    }, false);
    this.addEvent(this.audio, "canplay", function() {
      window.debugCTMPlayer("'canplay' event triggered.");
    }, false);
    this.addEvent(this.audio, "canplaythrough", function() {
      window.debugCTMPlayer("'canplaythrough' event triggered.");
    }, false);
    this.addEvent(this.audio, "durationchange", function() {
      window.debugCTMPlayer("'durationchange' event triggered.");
      self.detectMediaMode();
      if (self.audio.mode === "static") {
        self.container.classList.add("static");
        self.container.querySelector(".ctm-player-stop").removeAttribute("hidden");
        self.container.querySelector(".ctm-player-stop").setAttribute("disabled", "");
        if (!self.audio.paused) {
          self.container.querySelector(".ctm-player-stop").removeAttribute("disabled");
        }
      } else {
        self.container.querySelector(".ctm-player-stop").removeAttribute("disabled");
      }
    }, false);
    this.addEvent(this.audio, "emptied", function() {
      window.debugCTMPlayer("'emptied' event triggered.");
    }, false);
    this.addEvent(this.audio, "ended", function() {
      window.debugCTMPlayer("'ended' event triggered.");
      if (self.audio.wasPlaying && self.audio.mode === "stream") {
        self.resetPlayer();
      } else if (self.audio.mode === "static") {
        self.audio.currentTime = 0;
        self.container.querySelector(".ctm-player-play").removeAttribute("hidden");
        self.container.querySelector(".ctm-player-pause").setAttribute("hidden", "");
        self.container.querySelector(".ctm-player-stop").setAttribute("disabled", "");
      }
    }, false);
    this.addEvent(this.audio, "error", function() {
      window.debugCTMPlayer("'error' event triggered.");
      if (self.container.querySelector(".ctm-player-progress-bar")) {
        self.container.querySelector(".ctm-player-progress-bar").style.width = "0%";
        self.container.querySelector(".ctm-player-progress-bar").classList.remove("active");
      }
      if (self.audio.wasPlaying) {
        setTimeout(function() {
          self.resetPlayer();
        }, 5000);
      } else {
        self.container.querySelector(".ctm-player-stop").setAttribute("hidden", "");
        self.container.querySelector(".ctm-player-play").removeAttribute("hidden");
      }
    }, false);
    this.addEvent(this.audio, "loadeddata", function() {
      window.debugCTMPlayer("'loadeddata' event triggered.");
      self.updateProgress();
    }, false);
    this.addEvent(this.audio, "loadedmetadata", function() {
      window.debugCTMPlayer("'loadedmetadata' event triggered.");
    }, false);
    this.addEvent(this.audio, "loadstart", function() {
      window.debugCTMPlayer("'loadstart' event triggered.");
    }, false);
    this.addEvent(this.audio, "paused", function() {
      window.debugCTMPlayer("'paused' event triggered.");
    }, false);
    this.addEvent(this.audio, "play", function() {
      window.debugCTMPlayer("'play' event triggered.");
    }, false);
    this.addEvent(this.audio, "playing", function() {
      window.debugCTMPlayer("'playing' event triggered. Starting playback...");
      if (self.audio.mode === "stream") {
        if (self.container.querySelector(".ctm-player-progress-bar")) {
          self.container.querySelector(".ctm-player-progress-bar").classList.remove("active", "ctm-player-playing-static");
          self.container.querySelector(".ctm-player-progress-bar").style.width = "0%";
        }
      } else if (self.audio.mode === "static") {
        if (self.container.querySelector(".ctm-player-progress-bar")) {
          self.container.querySelector(".ctm-player-progress-bar").classList.add("ctm-player-playing-static");
          self.container.querySelector(".ctm-player-progress-bar").classList.remove("active");
        }
        if (self.container.querySelector(".ctm-player-progress")) {
          self.container.querySelector(".ctm-player-progress").classList.remove("ctm-player-progress-streaming");
        }
      }
      self.audio.wasPlaying = true;
    }, false);
    this.addEvent(this.audio, "progress", function() {
      window.debugCTMPlayer("'progress' event triggered.");
      if (self.audio.mode === "static") {
        self.updateProgress();
        // Adding a small delay to update, because in some browsers weird
        // implementations the last time progress is triggered is actually the
        // second to last event
        setTimeout(function() {
          self.updateProgress();
        }, 500);
      } else {
        self.updateProgress();
      }
    }, false);
    this.addEvent(this.audio, "ratechange", function() {
      window.debugCTMPlayer("'ratechange' event triggered.");
    }, false);
    this.addEvent(this.audio, "seeked", function() {
      window.debugCTMPlayer("'seeked' event triggered.");
    }, false);
    this.addEvent(this.audio, "seeking", function() {
      window.debugCTMPlayer("'seeking' event triggered.");
    }, false);
    this.addEvent(this.audio, "suspend", function() {
      window.debugCTMPlayer("'suspend' event triggered.");
    }, false);
    this.addEvent(this.audio, "stalled", function() {
      window.debugCTMPlayer("'stalled' event triggered.");
      if (self.audio.wasPlaying && self.audio.mode === "stream") {
        self.resetPlayer();
      }
    }, false);
    this.addEvent(this.audio, "timeupdate", function() {
      window.debugCTMPlayer("'timeupdate' event triggered.");
      self.formatTime();
    }, false);
    this.addEvent(this.audio, "volumechange", function() {
      window.debugCTMPlayer("'volumechange' event triggered.");
      if (self.audio.volume != self.container.querySelector(".ctm-player-volume-slider").value) {
        self.container.querySelector(".ctm-player-volume-slider").value = self.audio.volume;
      }
      if ((self.audio.muted && self.container.querySelector(".ctm-player-volume-button i").classList.contains("icon-volume-up")) || (!self.audio.muted && self.container.querySelector(".ctm-player-volume-button i").classList.contains("icon-volume-off"))) {
        self.container.querySelector(".ctm-player-volume-button i").classList.toggle("icon-volume-off");
        self.container.querySelector(".ctm-player-volume-button i").classList.toggle("icon-volume-up");
      }
    }, false);
    this.addEvent(this.audio, "waiting", function() {
      window.debugCTMPlayer("'waiting' event triggered.");
      if (self.container.querySelector(".ctm-player-progress-bar")) {
        self.container.querySelector(".ctm-player-progress-bar").style.width = "100%";
        self.container.querySelector(".ctm-player-progress-bar").classList.add("active");
      }
      if (self.container.querySelector(".ctm-player-progress")) {
        self.container.querySelector(".ctm-player-progress").classList.add("ctm-player-progress-streaming");
      }
    }, false);
  },
  formatTime: function() {
    let time = this.audio.currentTime,
      d = 0,
      h = 0,
      m = 0,
      s = 0,
      days_container = this.container.querySelector(".ctm-player-days"),
      hours_container = this.container.querySelector(".ctm-player-hours"),
      minutes_container = this.container.querySelector(".ctm-player-minutes"),
      seconds_container = this.container.querySelector(".ctm-player-seconds");
    if (time >= 86400 && days_container && hours_container && minutes_container && seconds_container) {
      d = Math.floor(time / 86400);
      time = time - d * 86400;
      h = Math.floor(time / 3600);
      time = time - h * 3600;
      m = Math.floor(time / 60);
      s = Math.floor(time % 60);
      days_container.style.display = "";
      days_container.textContent = d + "d";
      hours_container.style.display = "";
      hours_container.textContent = h.lead0(2) + ":";
      minutes_container.textContent = m.lead0(2) + ":";
      seconds_container.textContent = s.lead0(2);
    } else if (time >= 3600 && hours_container && minutes_container && seconds_container) {
      h = Math.floor(time / 3600);
      time = time - h * 3600;
      m = Math.floor(time / 60);
      s = Math.floor(time % 60);
      hours_container.style.display = "";
      hours_container.textContent = h.lead0(2) + ":";
      minutes_container.textContent = m.lead0(2) + ":";
      seconds_container.textContent = s.lead0(2);
    } else if (time >= 60 && minutes_container && seconds_container) {
      m = Math.floor(time / 60);
      s = Math.floor(time % 60);
      minutes_container.textContent = m.lead0(2) + ":";
      seconds_container.textContent = s.lead0(2);
    } else if (days_container && hours_container && minutes_container && seconds_container) {
      s = Math.floor(time % 60);
      days_container.style.display = "none";
      hours_container.style.display = "none";
      minutes_container.textContent = "00:";
      seconds_container.textContent = s.lead0(2);
    }
    if (this.audio.mode === "static") {
      if (this.container.querySelector(".ctm-player-progress-bar")) {
        this.container.querySelector(".ctm-player-progress-bar").style.width = this.audio.currentTime / this.audio.duration * 100 + "%";
      }
    }
  },
  updateProgress: function() {
    let progress = this.container.querySelector(".ctm-player-progress-download");
    if (progress !== null) {
      let ranges = [];
      for (let i = 0; i < this.audio.buffered.length; i++) {
        ranges.push([this.audio.buffered.start(i), this.audio.buffered.end(i)]);
      }
      while (progress.querySelectorAll("span").length < this.audio.buffered.length) {
        progress.appendChild(document.createElement("span"));
      }
      while (progress.querySelectorAll("span").length > this.audio.buffered.length) {
        progress.removeChild(progress.lastChild);
      }
      let spans = progress.querySelectorAll("span");
      for (let i = 0; i < this.audio.buffered.length; i++) {
        spans[i].style.left = (100 / this.audio.duration) * ranges[i][0] + "%";
        spans[i].style.width = (100 / this.audio.duration) * (ranges[i][1] - ranges[i][0]) + "%";
      }
    }
  },
  resetPlayer: function() {
    window.debugCTMPlayer("Playback has been stopped.");
    let previous = {
      volume: this.audio.volume,
      wasPlaying: this.audio.wasPlaying,
      mode: this.audio.mode
    };
    this.audio.pause();
    this.removeAllEvents(this.audio);
    if (this.container.querySelector(".ctm-player-progress-bar")) {
      this.container.querySelector(".ctm-player-progress-bar").style.width = "0%";
      this.container.querySelector(".ctm-player-progress-bar").classList.remove("active");
    }
    if (this.container.querySelector(".ctm-player-days")) {
      this.container.querySelector(".ctm-player-days").textContent = "";
    }
    if (this.container.querySelector(".ctm-player-hours")) {
      this.container.querySelector(".ctm-player-hours").textContent = "";
    }
    if (this.container.querySelector(".ctm-player-minutes")) {
      this.container.querySelector(".ctm-player-minutes").textContent = "00:";
    }
    if (this.container.querySelector(".ctm-player-seconds")) {
      this.container.querySelector(".ctm-player-seconds").textContent = "00";
    }
    // Modifying src string makes the browser to look for the new source, this
    // is used here to force stop download and streaming of the old source
    this.audio.src = "";
    // Listing and saving all attributes in a plain object, because "attributes"
    // obj is LIVE and of type NamedNodeMap, meaning that 1)the moment the
    // element is destroyed its "attributes" obj are also deleted and 2)the
    // "attributes" obj can't be cycled safely ('cause it can mutate at any time)
    var attrs = {};
    Array.prototype.slice.call(this.audio.attributes).forEach(function(item) {
      attrs[item.name] = item.value;
    });
    if (this.audio.mode === "stream") {
      let url = new URL(this.source),
        urlParams = new URLSearchParams(url.search);
      urlParams.set("cb", new Date().getTime());
      url.search = urlParams.toString();
      this.source = url.toString();
    }
    // Adding src as a listed attribute prevents the browser to load the new
    // source immediately until html is created
    attrs.src = this.source;
    let new_audio_element = document.createElement("audio");
    for (const [key, value] of Object.entries(attrs)) {
      if (key === "preload") {
        switch (this.audio.mode) {
        case "stream":
          new_audio_element.setAttribute(key, "none");
          break;
        case "static":
          new_audio_element.setAttribute(key, "metadata");
          break;
        }
      } else {
        new_audio_element.setAttribute(key, value);
      }
    }
    // Removing old audio element
    this.container.querySelector(".ctm-player-html5-player").removeChild(this.audio);
    this.container.querySelector(".ctm-player-html5-player").appendChild(new_audio_element);
    this.audio = this.container.querySelector(".ctm-player-html5-player audio");
    this.audio.volume = previous.volume;
    this.audio.wasPlaying = previous.wasPlaying;
    this.audio.mode = previous.mode;
    this.addEvents();
    if (this.container.querySelector(".ctm-player-volume-button i")) {
      this.container.querySelector(".ctm-player-volume-button i").classList.remove("icon-volume-off");
      this.container.querySelector(".ctm-player-volume-button i").classList.add("icon-volume-up");
    }
    this.container.querySelector(".ctm-player-play").removeAttribute("hidden");
    if (this.container.querySelector(".ctm-player-pause")) {
      this.container.querySelector(".ctm-player-pause").setAttribute("hidden", "");
    }
    this.container.querySelector(".ctm-player-stop").setAttribute("hidden", "");
    window.debugCTMPlayer("Player restarted in " + this.audio.mode + " mode.");
    if (this.audio.wasPlaying) {
      // Resetting buttons
      if (this.audio.mode === "static") {
        this.container.querySelector(".ctm-player-pause").removeAttribute("hidden");
        this.container.querySelector(".ctm-player-stop").removeAttribute("hidden");
      } else {
        this.container.querySelector(".ctm-player-play").setAttribute("hidden", "");
        this.container.querySelector(".ctm-player-stop").removeAttribute("hidden");
      }
      // Triggering a click event to resume playback
      let clickEvent = document.createEvent("HTMLEvents");
      clickEvent.initEvent("click", true, false);
      this.container.querySelector(".ctm-player-play").dispatchEvent(clickEvent);
    }
  },
  detectMediaMode: function() {
    // Detecting which type of audio is going to to be played. No variables are
    // set to store this.audio.duration so as not to waste memory each time the
    // durationchange event is fired
    if (this.audio.mode === undefined) {
      // If duration is Not-A-Number, then is a audio stream
      if (isNaN(this.audio.duration)) {
        this.audio.mode = "stream";
        console.info("An audio stream has been detected.", this);
        // Else if duration is a number, then is a static audio file
      } else {
        this.audio.mode = "static";
        console.info("A fixed duration audio has been detected.", this);
      }
    }
  },
  prepareUnloadPlayer: function() {
    let self = this,
      stopButton = this.container.querySelector(".ctm-player-stop");
    window.addEventListener("beforeunload", function() {
      if ((self.audio.mode === "stream" && !stopButton.hasAttribute("hidden")) || (self.audio.mode === "static" && !stopButton.hasAttribute("hidden") && !stopButton.hasAttribute("disabled"))) {
        var clickEvent = document.createEvent("HTMLEvents");
        clickEvent.initEvent("click", true, false);
        console.log("Stopping an active player...", self);
        self.container.querySelector(".ctm-player-stop").dispatchEvent(clickEvent);
      }
    });
  }
};

console.info("CTM Player v" + window.CTMPlayer.version + " - https://gitlab.com/breadmaker/ctm-player");
console.info("Run \"CTMPlayer.debug = true;\" to show debug messages.");

ready(function() {
  // Global events to hide dropdowns if clicked away from activator buttons
  document.addEventListener("click", function(evt) {
    let open_menu = document.querySelector(".ctm-player-source-select:not(hidden)");
    if (open_menu) {
      let parent = open_menu.closest(".ctm-player-container");
      if (!parent.querySelector(".ctm-player-config").contains(evt.target) && !parent.querySelector(".ctm-player-source-select").contains(evt.target)) {
        parent.querySelector(".ctm-player-source-select").setAttribute("hidden", "");
      }
    }
  });
  var index = 1;
  document.querySelectorAll("audio[data-extend='ctm-player']").forEach(function(el) {
    // If the element does not have and ID, assign one
    if (!el.hasAttribute("id")) {
      el.id = "audio_" + index;
      index += 1;
    }
    if (el.hasAttribute("data-classname")) {
      new window.CTMPlayer({
        selector: el.id,
        className: el.hasAttribute("data-classname")
      });
    } else {
      new window.CTMPlayer({
        selector: el.id
      });
    }
  });
});
