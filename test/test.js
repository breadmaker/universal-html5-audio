// Warning: This test script takes advantage of the ready function defined in
// the player js, take that into account if you want to copy this code.
var test_player = new CTMPlayer({
  selector: "test"
});
CTMPlayer.debug = true;

function resizeConsole() {
  var con = document.getElementById("ctm-player-console"),
    titleStyles = getComputedStyle(document.getElementById("test-title")),
    accordionStyles = getComputedStyle(document.getElementById("debug-accordion"));
  con.style.height = (window.innerHeight - (document.getElementById("test-title").clientHeight + parseInt(titleStyles.marginTop) + parseInt(titleStyles.marginBottom) + document.getElementById("ctm-player-test").clientHeight + document.getElementById("debug-accordion-header").clientHeight) + parseInt(accordionStyles.marginTop) - 25) + "px";
}

ready(function() {
  resizeConsole();
  //   var currentUrl = "http://stream.radio.gnuve.org:8087/listen.ogg";
  //   // Capturing submit event
  //   document.getElementById("test-url-form").addEventListener("submit", function(event) {
  //     // Preventing page load
  //     event.preventDefault();
  //     let url = document.getElementById("test-url-input").value;
  //     // If the input is not empty...
  //     if (url) {
  //       try {
  //         // ...Validate the input as an URL
  //         var testURL = new URL(url);
  //         if (url != currentUrl) {
  //           if (testURL.pathname.split(".")[1] == "ogg") {
  //             document.getElementById("test-url-help").classList.remove("d-none");
  //             document.getElementById("test-url-input-group").classList.remove("is-invalid");
  //             document.getElementById("test-url-input").classList.remove("is-invalid");
  //             currentUrl = test_player.audio.src = url;
  //             test_player.resetPlayer();
  //           } else {
  //             document.getElementById("test-url-help").classList.add("d-none");
  //             document.getElementById("test-url-input-group").classList.add("is-invalid");
  //             document.getElementById("test-url-input").classList.add("is-invalid");
  //           }
  //         }
  //       } catch {
  //         // Display an error message here
  //         document.getElementById("test-url-help").classList.add("d-none");
  //         document.getElementById("test-url-input-group").classList.add("is-invalid");
  //         document.getElementById("test-url-input").classList.add("is-invalid");
  //         return;
  //       }
  //     } else {
  //       // If input is empty, load the default url
  //       document.getElementById("test-url-help").classList.remove("d-none");
  //       document.getElementById("test-url-input-group").classList.remove("is-invalid");
  //       document.getElementById("test-url-input").classList.remove("is-invalid");
  //       if (currentUrl != "http://stream.radio.gnuve.org:8087/listen.ogg") {
  //         currentUrl = test_player.audio.src = "http://stream.radio.gnuve.org:8087/listen.ogg";
  //         test_player.resetPlayer();
  //       }
  //     }
  //   });
  window.addEventListener("resize", () => {
    resizeConsole()
  });
});
